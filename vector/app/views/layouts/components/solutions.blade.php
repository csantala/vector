<?php
        // get latest 5 articles
        $articles = DB::table('articles')->orderBy('id', 'desc')->take(5)->get();
   ?>
                <!-- Blog Categories Well -->
                <div class="well">
                    <h4>Recent Articles</h4>
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                    <?php if (isset($articles[$i])) { ?> 
                                <li><a href="/article/{{$articles[$i]->slug}}">{{$articles[$i]->title}}</a>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="col-lg-6">
                            <ul class="list-unstyled">
                                <?php for ($i = 0; $i < 3; $i++) { ?>
                                    <?php if (isset($articles[$i + 3])) { ?> 
                                <li><a href="/article/{{$articles[$i+3]->slug}}">{{$articles[$i+3]->title}}</a>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                     </div>
                    <!-- /.row -->
                </div>