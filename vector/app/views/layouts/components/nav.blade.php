    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Vector</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        @if (! Auth::check())<a href="http://{{$_SERVER['SERVER_NAME']}}/signin">Signin</a>@else<a href="http://{{$_SERVER['SERVER_NAME']}}/signout">Signout</a>@endif  
                    </li>
                    <!--li>
                        <a href="http://{{$_SERVER['SERVER_NAME']}}/services">Services</a>
                    </li-->
                    <li>
                        <a href="http://{{$_SERVER['SERVER_NAME']}}/about">About</a>
                    </li>                    
                    <li>
                        <a href="http://{{$_SERVER['SERVER_NAME']}}/contact">Contact</a>
                    </li>  
                    </ul>
                    <ul class="nav navbar-nav pull-right""> 
                    <li>
                        <a href="/theme/twilli_air">TwilliAir</a>
                    </li>
                    <li>
                        <a href="/theme/simplex">Simplex</a>
                    </li>
                    <li>
                        <a href="/theme/cosmos">Cosmos</a>
                    </li>
                    <li>
                        <a href="/theme/amelia">Amelia</a>
                    </li>
                    </li>
                    <li>
                        <a href="/theme/blog-home">Black</a>
                    </li>
                </ul>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>