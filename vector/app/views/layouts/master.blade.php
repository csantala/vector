<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Vector</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{Theme::asset('css/bootstrap.css');}}" rel="stylesheet">
    <link href="{{Theme::asset('css/bootswatch.min.css');}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{Theme::asset('css/blog-post.css');}}" rel="stylesheet">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

@include('layouts.components.nav')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

          <!-- Blog Post -->
                @yield('content')

            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">

                <!-- Blog Search Well -->
                <!--div class="well">
                    <h4>Blog Search</h4>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>
                    <!-- /.input-group -->
                <!-- /div-->
@include('layouts.components.sidewidget')

@include('layouts.components.solutions')


            </div>

        </div>
        <!-- /.row -->

        <hr>
@include('layouts.components.footer')
    </div>
    <!-- /.container -->
    
      <script src="{{asset('assets/js/detectmobilebrowser.js')}}"></script>
    <script src="{{Theme::asset('js/jquery-1.11.0.js');}}"></script>

    <script src="{{Theme::asset('js/bootstrap.min.js');}}"></script>
    
        <!-- Custom functions for this theme -->
    <script src="{{asset('assets/js/functions.min.js')}}"></script>
    <script src="{{asset('assets/js/initialise-functions.js')}}"></script>

    <script type="{{asset('js/script.js')}}"></script>

 

</body>

</html>
