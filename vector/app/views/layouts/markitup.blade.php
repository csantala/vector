<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>vector</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{Theme::asset('css/bootstrap.css');}}" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="{{Theme::asset('css/blog-post.css');}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
         <script src="../js/jquery-1.10.2.js"></script>
     <script src="../js/jqueryui.js"></script>

    <!-- Custom styles for this template -->
    <link href="../assets/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../js/markitup/skins/markitup/style.css" />
    <link rel="stylesheet" type="text/css" href="../js/markitup/sets/xbbcode/style.css" />
        <script type="text/javascript" src="../js/ckeditor/ckeditor.js"></script>
    <script href="{{ asset('js/ckeditor/ckeditor.js') }}" rel="text/javascript"></script>
        <!-- Custom styles for this template -->
    <link href="../assets/css/styles.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../js/markitup/skins/markitup/style.css" />
    <link rel="stylesheet" type="text/css" href="../js/markitup/sets/xbbcode/style.css" />
    
    <!-- markitup editor -->
    <script type="text/javascript" src="../js/markitup/jquery.markitup.js"></script>
    <script type="text/javascript" src="../js/markitup/sets/xbbcode/set.js"></script>    
<script>
$(document).ready(function() {
    $('#reditor').markItUp(myBbcodeSettings);

    $('#emoticons a').click(function() {
        emoticon = $(this).attr("title");
        $.markItUp( { replaceWith:emoticon } );
    });
});
</script>

</head>

<body>

@include('layouts.components.nav')

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->
                @yield('content')

            </div>
        </div>
        <!-- /.row -->

        <hr>

@include('layouts.components.footer')

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
 <script type="text/javascript"  src="../js/script.js"></script>
</body>

</html>
