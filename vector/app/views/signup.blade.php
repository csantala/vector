@extends('layouts.account')
@section('content')
              <h1 class="section-title">Sign Up</h1>

                <!-- SIGNUP FORM -->
                <div class="col-sm-7 col-md-12">

                  <form class="form-style clearfix" action="/signup" method="post">

                    <!-- form left col -->
                    <div class="col-md-6"> 
                      <div class="form-group">
                        <input type="email" class="text-field form-control" placeholder="Email Address" name="email" value="{{Input::get('email')}}">
                      </div>
                      <div class="form-group">
                        <input type="text" class="text-field form-control"  placeholder="Username" name="name" value="{{Input::get('name')}}">
                      </div>
                      <div class="form-group">
                        <input type="password" class="text-field form-control"  placeholder="Password" name="password" value="{{Input::get('password')}}">
                      </div>
                       <div class="form-group">
                        <input type="password" class="text-field form-control" placeholder="Password (again)" name="password_confirmation" value="{{Input::get('password_confirmation')}}">
                      </div>                  
                    </div><!-- end: form left col -->

                    <!-- form right col -->
                    <div class="col-md-6">

                      <div class="form-group">
                        <!--img src="assets/images/theme_images/loader-form.GIF" class="form-loader"-->
                        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                      
                      </div> 
                       <p>Already have an account! <a href="signin">Sign In Here</a></p> 
                      <div class="warning">
                      <?php if (! empty($errors)): ?>

						<span style="color:#FF1D1B">@foreach ($errors as $error) <p>{{ $error }}</p>@endforeach</span>

					<?php endif ?>	
                      </div>           
                    </div><!-- end: form right col -->

                  </form>
                </div><!-- end: CONTACT FORM -->
                
@stop