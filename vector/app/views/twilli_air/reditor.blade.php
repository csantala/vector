@extends('layouts.markitup')
@section('content')
<form action="/updater/{{$article->id}}" method="post">
	 <div class="form-group">
		<p><label for="title">Title</label>
		<input type="title" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{$article->title}}">
		</p>
  	</div>
  	
 <div id="emoticons">
    <a href="#" title=":p"><img src="../js/markitup/sets/xbbcode/images/emoticon-happy.png" /></a>
    <a href="#" title=":("><img src="../js/markitup/sets/xbbcode/images/emoticon-unhappy.png" /></a>
    <a href="#" title=":o"><img src="../js/markitup/sets/xbbcode/images/emoticon-surprised.png" /></a>
    <a href="#" title=":p"><img src="../js/markitup/sets/xbbcode/images/emoticon-tongue.png" /></a>
    <a href="#" title=";)"><img src="../js/markitup/sets/xbbcode/images/emoticon-wink.png" /></a>
    <a href="#" title=":D"><img src="../js/markitup/sets/xbbcode/images/emoticon-smile.png" /></a>
</div> 	
  	
    <div class="form-group">
    	<label for="title">Resources</label>
		<textarea id="reditor" rows="30" cols="80" name="resources">{{$article->resources}}</textarea>
		<input type="hidden" name="article_id" value="{{$article->id}}" />
    </div>
    {{ Form::select('size', array('L' => 'Large', 'S' => 'Small'), 'S'); }}
     <button type="submit" class="btn btn-default">Post</button>
</form>
@stop