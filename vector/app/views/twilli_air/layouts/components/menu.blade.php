        <ul id="main-menu">
          @if (Auth::check())<li class="menu-item"><a href="/newarticle">new article</a></li>@endif
          <li id="menu-item-text" class="menu-item scroll"><a href="#text">featured</a></li>
          <li id="menu-item-grid" class="menu-item scroll"><a href="#grid">grid</a></li>
          <li id="menu-item-contact" class="menu-item scroll"><a href="#contact">Contact</a></li>
          <br>
          <li id="menu-item-simplex" class="menu-item"><a href="/theme/simplex">Simplex</a></li>
           <li id="menu-item-cosmos" class="menu-item"><a href="/theme/cosmos">Cosmos</a></li>
          <li id="menu-item-amelia" class="menu-item"><a href="/theme/amelia">Amelia</a></li>
          <li id="menu-item-bloghome" class="menu-item"><a href="/theme/blog-home">Blog-Home</a></li>
        </ul><!-- #main-menu -->
