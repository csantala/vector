@extends('layouts.master')
@section('content')
           @foreach ($articles as $article) 
			<div class="col-lg-8">
                <h3 class="section-title">{{$article->title}}</h3>
                <span class="glyphicon glyphicon-time posted_on"></span> <span class="posted_on">Posted on {{$article->date}}</span>
                <p class="feature-paragraph">
                        {{ BBCode::parse(Str::words($article->body, 50, '..')) }}
                </p>
                <p>
                    <a href="/article/{{$article->slug}}" class="fade_out_body btn btn-outline-inverse btn-sm">comment</a>&nbsp;
                    <a href="" onclick="populate_and_open_modal(event, 'modal-content-1r{{$article->id}}');" class="btn btn-outline-inverse btn-sm">solutions</a>
                    @if($sysop)
                        <a class="btn btn-outline-inverse btn-sm" href="edit/{{$article->id}}">edit</a>
                        @if($delete_button)<a class="confirm btn btn-outline-inverse btn-sm" href="delete/{{$article->id}}">delete</a>@endif   
                    @endif
                </p>
                <!-- #modal-content-1 -->             
                           <p><br> <hr></p>
            </div>
            
            <div class="content-to-populate-in-modal" id="modal-content-1r{{$article->id}}"> 
                {{BBCode::parse($article->resources)}}
                 @if($sysop)
                <p><a href="/editorr/{{$article->id}}" class="btn btn-outline-inverse btn-sm">edit</a></p>
                @endif
            </div>
           @endforeach
@stop

@section('grid')
<article id="grid" class="section-wrapper clearfix" data-custom-background-img="assets/images/other_images/bg1.jpg">
          <div class="content-wrapper clearfix">
            <div class="col-lg-8">

                <h1 class="section-title">Grid</h1>
                
                <!-- grid -->
                <section class="grid row clearfix clearfix-for-2cols">

                  @foreach ($grid_articles as $article)
                  <!-- grid item -->
                  <div class="grid-item col-md-6">
                    <div class="item-content clearfix">
                      <span class="icon glyphicon glyphicon-{{$article->grid_icon}}"></span>
                      <div class="text-content">
                        <h5>{{$article->title}}</h5>
                        <p>{{Str::words($article->body, 10, '..');}}</p>
                        
                        <a href="article\{{$article->slug}}" class="fade_out_body btn btn-outline-inverse btn-sm">read</a>
                         <!--if ($first_article_resources)-->
                         <a href="" onclick="populate_and_open_modal(event, 'modal-content-{{$article->id}}r');" class="btn btn-outline-inverse btn-sm">*</a>
                         <!--endif-->
                      </div>
                    </div><!-- end: .item-content -->
                    <div class="content-to-populate-in-modal" id="modal-content-{{$article->id}}r">   
                    <h3 class="section-title">{{$article->title}}</h3>
                    <span class="glyphicon glyphicon-time posted_on"></span> <span class="posted_on">Posted on {{$article->date}}</span>     
                    {{BBCode::parse($article->resources)}} @if ($sysop)<a class="btn btn-outline-inverse btn-sm" href="editorr/{{$article->id}}">edit</a><a class="confirm btn btn-outline-inverse btn-sm" href="delete/{{$article->id}}">delete</a>@endif
                </div><!-- #modal-content-1 -->
                  </div><!-- end: .grid-item -->
                  @endforeach

                </section><!-- end: grid -->

            </div><!-- .col-sm-11 -->
          </div><!-- .content-wrapper -->
        </article><!-- .section-wrapper -->
@stop