@extends('layouts.tracker')
@section('content')

                <!-- the actual blog post: title/author/date/content -->
                <h1>Tracker</h1>
					<table class="table">
						<thead>
							<th class="width:200">Date</th>
							<th>Page</th>
							<th>Referrer</th>
							<th>IP</th>
							<th>Platform</th>
							<th>Device</th>
							<th>Browser</th>
						</thead>
						<tbody>
							@foreach ($visitors as $visitor)
							<tr>
								<td>{{ date("d", strtotime($visitor->created_at)) }}</td>
								<td>{{ $visitor->page }}</td>
								<td><a href="{{$visitor->referrer}}">{{ substr($visitor->referrer, 7, 10) }}</a></td>
								<td><a href="http://www.infosniper.net/?ip_address={{ $visitor->IP }}" target="_blank">{{ $visitor->IP }}</a></td>
								<td>{{ $visitor->platform }}</td>
								<td>{{ $visitor->device }}</td>
								<td>{{ $visitor->browser }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
                <hr>
     
@stop