@extends('layouts.edit')
@section('content')
<form action="/{{$action}}/{{$article_id}}" method="post">
	 <div class="form-group">
		<p><label for="title">Title</label>
		<input type="title" class="form-control" id="title" name="title" placeholder="Enter Title" value="{{$article_title}}">
		</p>
  	</div>
    <div class="form-group">
    	<label for="title">Article</label>       
		<textarea id="body" rows="30" cols="80" name="article">{{$article_body}}</textarea>
    </div>

    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Select icon <span id="selected_icon" class="caret"></span>
        </button>
        <ul class="dropdown-menu list-inline" role="menu" style="background-color:#aaa">
        	<li data-value="cutlery"><span class="glyphicon glyphicon-cutlery"></span></li>
            <li data-value="fire"><span class="glyphicon glyphicon-fire"></span></li>
            <li data-value="glass"><span class="glyphicon glyphicon-glass"></span></li>
            <li data-value="heart"><span class="glyphicon glyphicon-heart"></span></li>          
        </ul>
    </div>
    <input type="hidden" name="gricon" id="gricon">

</span>
     <button type="submit" class="btn btn-default">Post</button>
</form>


    <script>
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace( 'body' );
    </script>
@stop



