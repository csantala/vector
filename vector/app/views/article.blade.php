@extends('layouts.article')
@section('content')
	
                <h1 class="section-title">{{$article_title}}</h1>
				<span style="color:#ccc"> <span class="glyphicon glyphicon-time"></span> Posted on {{$article_date}}</span>
                <p class="feature-paragraph">
    				{{$article_body}} 
				</p>
				<p>
					@if($sysop)
						<a class="btn btn btn-default btn-sm" href="../edit/{{$article_id}}">edit</a>
						@if($delete_button)<a class="confirm btn btn-danger btn-sm" href="delete/{{$article_id}}">delete</a>@endif	
					@endif
				</p>
        
             
               {{BBCode::parse($resources)}}
                 @if ($sysop)
                <a href="/editorr/{{$article_id}}" class="btn btn-default btn-sm">edit resources</a>
                @endif
  
                @if ($enable_comments)
                 <div id="disqus_thread"></div>
                <script type="text/javascript">
                    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                    var disqus_shortname = 'ablitica'; // required: replace example with your forum shortname
                    var disqus_identifier = '{{{$article_slug}}}';
                    var disqus_url = 'http://vector.ablitica.com/{{{$article_slug}}};';
            
                    /* * * DON'T EDIT BELOW THIS LINE * * */
                    (function() {
                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                </script>
                <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
			    @endif 
@stop

