    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Vector</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav pull-left">
                    <li><a href="#about">About</a>
                    </li>
                    <!--li><a href="#services">Services</a>
                    </li>
                    <li><a href="#contact">Contact</a>
                    </li-->
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li><a id="login-register">Login / Register</a>
                    	<div class="form-group form-inline hidden" id="login-form" style="padding-top:8px;height:10px;">
							<?=Form::open(array('url' => 'account/login')); ?>
	   						<?=Form::text('username', null, array('placeholder' => 'Username', 'id' => 'username', 'class' => 'form-control'));?>
	   						<?=Form::password('password', array('placeholder' => 'password', 'class' => 'form-control'));?>
							<?=Form::submit('Login', array('class' => 'btn btn-default'));?>
							<?=Form::close();?>	
						</div>
                    </li>
                </ul>
    
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>