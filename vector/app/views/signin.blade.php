@extends('layouts.account')
@section('content')
              <h1 class="section-title">Sign In</h1>
              <br>
            <div class="container-fluid">
                <!-- SIGNUP FORM -->
                <div class="col-sm-7 col-md-12">

                  <form class="form-style clearfix" action="/signin" method="post">

                    <!-- form left col -->
                    <div class="col-md-6"> 
                      <div class="form-group">
                        <input type="email" class="text-field form-control" placeholder="Email Address" name="email" value="phoenix@ablitica.com">
                      </div>
                      <div class="form-group">
                        <input type="password" class="text-field form-control" id="form-password" placeholder="Password" name="password" value="phoenix123">
                      </div>                   
                    </div><!-- end: form left col -->

                    <!-- form right col -->
                    <div class="col-md-6">

                      <div class="form-group">
                        <!--img src="assets/images/theme_images/loader-form.GIF" class="form-loader"-->
                        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
                        <br>
                       
                      </div> <p>Don't have an account! <a href="signup">Sign Up Here</a></p> 
                      <div class="warning">
                      <?php if (! empty($errors)): ?>

						<span style="color:#FF1D1B">@foreach ($errors as $error) <p>{{ $error }}</p>@endforeach</span>

					<?php endif ?>	
                      </div>           
                    </div><!-- end: form right col -->

                  </form>
                </div><!-- end: CONTACT FORM -->
       </div>         
@stop