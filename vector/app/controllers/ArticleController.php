<?php

class ArticleController extends BaseController {

 /**
  * The Article Controller.
  *
  * Prepares articles for rendering from article slug (created from article title).
  * 
  */
	protected $layout = 'layouts.article';

	// load article by title slug
	public function ShowArticle($slug)
	{
		$article = New Article;
		$sysop = false;
		$read_more = false;
		$user = Auth::check();
		$article = new Article;
		$delete_button = true;

		// get first article by slug from db
		$article = DB::table('articles')->where('slug', $slug)->first();
    
 		// tracker
		Session::flash('message', $slug);
		
		// TODO: update with user roles
		// sysop logged on?
        // sysop logged on?
        if (Auth::check() && Auth::user()->email == "csantala@gmail.com") 
        {
            $sysop = true;
        }
    
		$view_vars = array(
			'delete_button'		=> $delete_button,
			'enable_comments'	=> true,
			'article_title'		=> $article->title,
			'article_body'		=> $article->body,
			'article_id'		=> $article->id,
			'article_date'		=> $article->date,
			'article_slug'		=> $article->slug,
			'resources'			=> $article->resources,
			'sysop'				=> $sysop
		);
		return View::make('article', $view_vars);
	}
}