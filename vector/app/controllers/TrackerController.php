<?php

class TrackerController extends BaseController {
	
	// where you at, what you been doing? how can we do it better?
	public function index()
	{	
		$n = 100;
		$visitors = TinyTracker::orderBy('created_at', 'DESC')->take($n)->get();
		// track
		Session::flash('message', 'Tracker Page ');

		return View::make('tracker', array('visitors' => $visitors));
	}
}