<?php

 /**
 * The Account Controller.
 *  Laravel 4.2
 *  Eloquent ORM
 *  Twitter Bootstrap 2.3.2/Blade template engine
 *  Chris Santala 2014
 *
 * Creates accounts and authenticates users.
 * 
 * Renders signin or signup view with input and error messages.
 * Validates input. Creates, authorizes, and deauthorizes users.
 * Redirects to front page upon successful validation and authorization.
 * 
 * Routes/function:
 *  signin/index()
 *  signup/signup()
 *
 * Signup Validation Rules:
 *  required unique & proper email
 *  required username
 *  required 8 character password with confirmation
 *
 * Used at the experimental Vector Blog Platform at http://vector.ablitica.com
 * Complete code available in open source at https://bitbucket.org/csantala/vector
 */
 
class AccountController extends \BaseController {

	protected $layout = 'layouts.account';

	/**
	 * Signin.
	 * 
	 * Display signin form with input and error messages, validate input, authenticate user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$validator = null;
		$input = Input::all();
		$messages = null;
		$errors = array();
		$userdata = null;
		$view_vars = null;

		// track
		Session::flash('message', 'Signin Page ');

		// validate input as per rules in User model
		$validator = Validator::make($input, User::$signin_rules);

		// validation fails: set errors (rendered at signup view)
		if ($input && $validator->fails())
		{
			$messages = $validator->messages();
			$errors = $messages->all();
		}

		if ($input && $validator->passes())
		{		    
            try
            {
              Auth::attempt(array(
                 'identifier' => Input::get('email'),
                  'password' => Input::get('password')
              ));
               return Redirect::to('');
            }
            catch (Exception $e)
            {
              $errors = array($e->getMessage());
            }    
        }
		
		$view_vars = array(
			'errors' => $errors
		);

		return View::make('signin', $view_vars);	
	}
	
	/**
	 * Signup.
	 * 
	 * Displays signup form with input and error messages, validates input, authenticate new user with hashed password.
	 *
	 * @return Response
	 */
	public function signup()
	{
		$validator = null;
		$errors = array();
		$input = Input::all();
		$messages = null;		
		$user = null;
		$password = null;
		$view_vars = null;
		
		// track
		Session::flash('message', 'Signup Page ');

		// validate input, return validation errors & input to view when validation fails
		$validator = Validator::make($input, User::$rules);

		if ($input && $validator->fails())
		{
			$messages = $validator->messages();
			$errors = $messages->all();
		}
		
		if ($input && $validator->passes())
		{ 
            // Create a new Permission
            $permission = new Toddish\Verify\Models\Permission;
            $permission->name = 'user_article_crud';
            $permission->save();
            
            // Create a new Role
            $role = new Toddish\Verify\Models\Role;
            $role->name = 'user';
            $role->level = 1;
            $role->save();
            
            // Assign the Permission to the Role
            $role->permissions()->sync(array($permission->id));
            
            // Create a new User
            $user = new Toddish\Verify\Models\User;
            $user->username = Input::get('name');
            $user->email = Input::get('email');
            $user->password = Input::get('password'); // This is automatically salted and encrypted
            $user->verified = true;
            $user->save();
            
            // Assign the Role to the User
            $user->roles()->sync(array($role->id));
            			
           try
           {
              Auth::attempt(array(
                 'identifier' => Input::get('email'),
                 'password' => Input::get('password')
              ));
               return Redirect::to('');
            }
            catch (Exception $e)
            {
              $errors = array($e->getMessage());
            }
		}
		
		$view_vars = array(
			'errors' => $errors
		);
			
		return View::make('signup', $view_vars);	
	}

	/**
	 * Signout.
	 * 
	 * Deauthorizes user, returns to front page.
	 *
	 * @return Response
	 */
	public function signout()
	{
		Auth::logout();
		return Redirect::to('');
	}
}