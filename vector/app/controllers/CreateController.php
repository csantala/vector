<?php

class CreateController extends BaseController {

	protected $layout = 'layouts.master';
	private $sysop = null;
    
    public function __construct() 
    {
        $user = Auth::check();          


    }
    
	public function index()
	{
	    // sysop logged on?
        if (Auth::check() && Auth::user()->email == "csantala@gmail.com") 
        {
            $this->sysop = true;
            return View::make('create');
        }
        else
        { 
             return View::make('fourohfour');
        }

	}
	
}