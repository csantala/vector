<?php

 /**
  * The Edit Controller.
  *
  * Article/Resource CRUD by id or article title slug. CKEditor or Markitup editor views.
  * 
  */

class EditController extends BaseController {

	protected $layout = 'layouts.edit';
	protected $sysop = null;

	public function __construct() 
	{
		$user = Auth::check();			

        // sysop logged on?
        if (Auth::check() && Auth::user()->email == "phoenix@ablitica.com") 
        {
            $this->sysop = true;
        }
        else die;
	}
	
	// edit article	
	public function index($id = null)
	{
        
		$article = Article::find($id);

		if (! $article)
		{
			$article = new stdClass();
			$article->title = '';
			$article->body = '';
			$article->id = 1;
		}
		$view_vars = array(
			'action' => 'update',
			'article_id' => $id,
			'article_title' => $article->title,
			'article_body' => $article->body,
		);
		return View::make('editor', $view_vars);
	}
    
	public function newArticle()
	{
		$action = 'post';
		$view_vars = array(
			'action' => 'post',
			'article_id' => null,
			'article_title' => '',
			'article_body' => ''
		);
		return View::make('editor', $view_vars);
	}

	public function postArticle()
	{ 
		$article = New Article;
		$article->date = date('Y',time());
		$article->title = $_POST['title'];
		$article->body = $_POST['article'];
		$article->grid_icon = $_POST['gricon'];
		$article->save();
		return Redirect::to('/');
	}
	
	public function update($id = null)
	{
		$this->sysop = true;
		$article = Article::find($id);
	
		// no articles in db
		if (! $article)
		{
			$article = new Article;
			$article->date = date('Y', time());
			$article->title = '';
			$article_id = 1;
			$article_body = '';
			$article_grid_icon = 'heart';
		} else {
			$article->slug = Str::slug($_POST['title']);
			$article->date = date('Y', time());
			$article->title = $_POST['title'];
			$article->body = $_POST['article'];
			$article->grid_icon = $_POST['gricon'];
		}
		
		if ($this->sysop) $article->save();
		return Redirect::to('/');
	}
	
	public function post() 
	{
		$article = new Article;
		$article->title = $_POST['title'];
		$article->body = $_POST['article'];
		if ($this->sysop) $article->save();
		return Redirect::to('/');
	}
	
	public function delete($id = null)
	{
		$article = Article::find($id);
		if ($article) { if ($this->sysop) $article->delete(); }
		return Redirect::to('/');
	}
    
    public function editResources($id = null)
    {
		$sysop = false;
		$delete_button = false;
        
        $user = Auth::check();
                
		$article = Article::find($id);
        
		// tracker
		Session::flash('edit resources', $id);
                 
		$view_vars = array(
			'delete_button' => $delete_button,
			'article'   	=> $article,
			'sysop'         => $this->sysop
		);

		return View::make('reditor', $view_vars);
	}
	
	public function updateResources($id = null)
	{
		
		$sysop = true;
		$enable_comments = false;
		
		$article = Article::find($id);
		
		// no articles in db
		if (! $article)
		{
			$article = new Article;
			$article->resources = '';
			$article_id = 1;
		}
		
		$view_vars = array(
			'article' => $article,
			'resources' => $article->resources,
			'article_id' => $article->id,
			'article_title' => $article->title,
			'article_body' => $article->body,
			'article_date' => $article->date,
			'sysop' => $sysop,
			'enable_comments' => $enable_comments
		);
        
		$article->resources = $_POST['resources'];
		
		if ($this->sysop) $article->save();
		return Redirect::to('/');
        
		// render article view
		return View::make('reditor', $view_vars);
		}
	
	public function deleteResources($id = null)
	{
		$article = Article::find($id);
		//if ($article) { if ($this->sysop) $article->delete(); }
		return Redirect::to('/');
	}	
}