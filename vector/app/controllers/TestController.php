<?php

class TestController extends BaseController {



    public function index_()
    {

    $highlighter = new \FSHL\Highlighter(new \FSHL\Output\Html(), \FSHL\Highlighter::OPTION_TAB_INDENT | \FSHL\Highlighter::OPTION_LINE_COUNTER);
    echo '<pre>';
    echo $highlighter->highlight('<?php echo "Hello world!"; ?>', new \FSHL\Lexer\Php());
    echo '</pre>';
    }

	public function index()
	{
	    Theme::init('cosmos', array(
            'public_dirname' => 'allthemes',    // Base dirname for contain all themes, relative to public path.
            'views_path' => app_path('views'),  // Change the path to contain theme templates.
        ));
        
       
		return View::make('test');
	}

	// polymer tinkerage
	public function index2()
	{
		return View::make('polymer_demo1');
	}

	// polymer tinkerage
	public function index3()
	{
		return View::make('polymer_demo2');
	}


	// angular js tinkerage
	public function index1()
	{
		return View::make('test_angular');
	}

}