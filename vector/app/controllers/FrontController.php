<?php

class FrontController extends BaseController {

 /**
  * The Front Controller.
  *
  * Gets recent last 5 articles and passed them to the front view. Receives and processes Contact Form email.
  * Tracks user activity.
  * 
  * Sysop hard log on for demo purposes.
  * 
  */

	protected $layout = 'layouts.master';
	
	public function showFront()
	{
		
		// demo mode, hard logged on as sysop
		// todo: remove, incorporate user roles, set this up as 'demo user' with partitioned access level rights.
		//$user = User::where('username', 'Chris')->first();
		//D::ds($user,1);
		//Auth::login($user);

		$articles = array();
		$article = New Article;
		$sysop = false;
		$read_more = false;
		$user = Auth::check();
		$article = new Article;
		$title = '';
		$body = '';
		$id = '';
		$slug = '';
		$delete_button = true;
		 
		// get latest 5 articles
		$articles = DB::table('articles')->orderBy('id', 'desc')->take(5)->get();
		
		// tracker
		Session::flash('message', 'Front Page ');
	
		// sysop logged on?
		if (Auth::check() && Auth::user()->email == "phoenix@ablitica.com") 
		{
			$sysop = true;
		}
		
		// prepare grid articles
		$articles_grid = $articles;

		array_shift($articles_grid);

		$view_vars = array(
		    'articles'              => $articles,
		    'grid_articles'         => $articles_grid,
			'delete_button'         => $delete_button,
			'read_more' 	        => $read_more,
			'enable_comments'       => true,
			'sysop'				    => $sysop
		); 
		return View::make('front', $view_vars);
	}
	
	// what's this doing here?
	public function edit() 
	{
		$article = new Article;
		$article->title = Input::get('title');
		$article->body = Input::get('article');
		$article->save();
		return Redirect::to('');
	}
    
	public function contact()
	{
		/* =====================================================
		 * change this to the email you want the form to send to
		 * ===================================================== */
		$email_to = "csantala@gmail.com";
		$email_subject = "Contact Form submitted";
        
		if(isset($_POST['email']))
		{
			function return_error($error)
			{
				echo $error;
				die();
			}
        
			// check for empty required fields
			if (!isset($_POST['name']) ||
			!isset($_POST['email']) ||
			!isset($_POST['message']))
			{
				return_error('Please fill in all required fields.');
			}
        
			// form field values
			$name = $_POST['name']; // required
			$email = $_POST['email']; // required
			$contact_number = $_POST['contact_number']; // not required
			$message = $_POST['message']; // required
        
			// form validation
			$error_message = "";
        
			// name
			$name_exp = "/^[a-z0-9 .\-]+$/i";
			if (!preg_match($name_exp,$name))
			{
				$this_error = 'Please enter a valid name.';
				$error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
			}        
        
			$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
			if (!preg_match($email_exp,$email))
			{
				$this_error = 'Please enter a valid email address.';
				$error_message .= ($error_message == "") ? $this_error : "<br/>".$this_error;
			} 
        
			// if there are validation errors
			if(strlen($error_message) > 0)
			{
				return_error($error_message);
			}
        
			// prepare email message
			$email_message = "Form details below.\n\n";
        
			function clean_string($string)
			{
				$bad = array("content-type", "bcc:", "to:", "cc:", "href");
				return str_replace($bad, "", $string);
			}
        
			$email_message .= "Name: ".clean_string($name)."\n";
			$email_message .= "Email: ".clean_string($email)."\n";
			$email_message .= "Contact number: ".clean_string($contact_number)."\n";
			$email_message .= "Message: ".clean_string($message)."\n";
        
			// create email headers
			$headers = 'From: '.$email."\r\n".
			'Reply-To: '.$email."\r\n" .
			'X-Mailer: PHP/' . phpversion();
			if (@mail($email_to, $email_subject, $email_message, $headers))
			{
				echo 'Email sent! '.date('D m/Y', time()); die;
			}
			else 
			{
			echo 'An error occured. Please try again later.';
				die();        
			}
		}
		else
		{
			echo 'Please fill in all required fields.';
			die();
		}
	}
}