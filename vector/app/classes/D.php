<?php
class D {
    public static function ds($var, $flag = false)
	{
        echo "<pre>";
		var_dump($var);
		echo "</pre>";
		if ($flag) { die; }
    }
}