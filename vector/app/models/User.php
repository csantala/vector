<?php

use Toddish\Verify\Models\User as VerifyUser;

class User extends VerifyUser
{
        protected $fillable = array('email', 'name', 'password');
    
    public static $rules = array(
        'name'      => 'required|max:50',
        'email'     => 'required|email|unique:users',
        'password'  => 'required|confirmed|min:8'
    );
    public static $signin_rules = array(
        'email'     => 'required',
        'password'  => 'required'
    );
}
 
