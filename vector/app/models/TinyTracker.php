<?php

	class TinyTracker extends Eloquent {
		protected $fillable = array('page', 'referrer', 'IP', 'platform', 'device', 'browser');	
	}
