<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// test
Route::get('/test', 'TestController@index');

// polymer test
Route::get('/polymer_demo1', 'TestController@index2');

// polymer test
Route::get('/polymer_demo2', 'TestController@index3');


// angular test
Route::get('/angular', 'TestController@index1');

// home page
Route::get('/', 'FrontController@showFront');

// services page
Route::get('/services', function(){
	// track
	Session::flash('message', 'Services Page ');
	return View::make('services');	
});

// about page
Route::get('/about', function(){
	// track
	Session::flash('message', 'About Page ');
	return View::make('about');	
});

// contact page
Route::get('/contact', function(){
	// track
	Session::flash('message', 'Contact Page ');
	return View::make('contact');	
});


// front page inline editing 
Route::post('/front/edit', 'FrontController@edit');
Route::post('/front/update', 'FrontController@update');

// get article by slug
Route::get('/article/{slug}', 'ArticleController@ShowArticle');

// js textarea editors
Route::any('/edit/', 'EditController@index'); // new article editor
Route::get('/newarticle', 'EditController@newArticle'); // edit new article in editor
Route::post('/post', 'EditController@postArticle'); // post new article from editor
Route::any('/edit/{id}', 'EditController@index'); // edit article of id in editor
Route::post('/update/{id}', 'EditController@update'); // updte article
Route::get('/delete/{id}', 'EditController@delete'); // from orbit

// markitup editor for resources CRUD
Route::get('/editorr/{id}', 'EditController@editResources');
Route::match(array('GET', 'POST'), '/updater/{id}', 'EditController@updateResources');
Route::get('/deleter/{id}', 'EditController@deleteResources');

// login
Route::match(array('GET', 'POST'), '/signin', 'AccountController@index');

// register
Route::match(array('GET', 'POST'), '/signup', 'AccountController@signup');

// contact
Route::match(array('GET', 'POST'), '/contact/go', 'FrontController@contact');

// logout
Route::get('/signout', 'AccountController@signout');

//Route::resource('account', 'AccountController');
Route::resource('create', 'CreateController');

// tracker
Route::group(array('after' => 'log'), function()
{
    Route::get('/', 'FrontController@showFront');
    Route::get('/article/{slug}', 'ArticleController@showArticle');
	Route::get('/signin', 'AccountController@index');
	Route::get('/signup', 'AccountController@signup');
});

// tracker test
Route::get('/tracker', 'TrackerController@index');

// quote test
Route::get('/quote', function(){
	echo Quotes::single();
});

Route::get('/theme/{theme}', function($theme)
{
    DB::table('config')->update(array('theme' => $theme));
    return Redirect::to('');
});