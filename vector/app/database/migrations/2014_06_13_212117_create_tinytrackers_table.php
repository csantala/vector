<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTinyTrackersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
   		Schema::create('tiny_trackers', function($table)
   		{
			$table->increments('id');
			$table->string('page', 128);
			$table->string('referrer');
			$table->string('IP', 15);
			$table->string('platform', 128);
			$table->string('device', 128);
			$table->string('browser',64);
			$table->timestamps();
   		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tinytracker');
	}

}
