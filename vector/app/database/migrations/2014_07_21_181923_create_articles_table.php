<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
   		Schema::create('articles', function($table)
   		{
			$table->increments('id');
			$table->string('date', 11);
			$table->string('title',512);
			$table->text('body');
			$table->text('tags');
			$table->string('slug', 512);
			$table->text('resources');
			$table->string('grid_icon',128);
			$table->timestamps();
   		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}