<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
    {
        Schema::create('config', function($table)
        {
            $table->increments('id');
            $table->string('theme', 128);
            $table->timestamps();
        });
        
        DB::table('config')->insert(array('theme' => 'twilli_air'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config');
    }

}
