$(function() {

  $(function() {
    $("li").click(function() {
    	var selected = $(this).attr('data-value');
    	 $("#selected_icon").attr("class",'glyphicon glyphicon-' + selected);

  
   		 $("#gricon").val($(this).attr('data-value'));
    });
});
    
    // placeholder type-over fix
  	$('input, textarea').on('focus',function() {
		if ($(this).attr('placeholder')) {
			$(this).data('placeholder', $(this).attr('placeholder')).removeAttr('placeholder');
			}
		}).on('blur', function() {
		if ($(this).data('placeholder')) { 
			$(this).attr('placeholder', $(this).data('placeholder')).removeData('placeholder');
		}
	}); 

	/*	
$(document).on("click", "a", function () {

    // get the href attribute
    var newUrl = $(this).attr("href");

    // veryfy if the new url exists or is a hash
    if (!newUrl || newUrl[0] === "#") {
        // set that hash
        location.hash = newUrl;
        return;
    }

    // now, fadeout the html (whole page)
   	var overlay = jQuery('<div id="overlay"> </div>');
	overlay.appendTo(document.body);
	$('#overlay').animate({
       opacity: 1,
     }, 300, function() {
                location = newUrl;

     });

    // prevent the default browser behavior.
    return false;
});	
	
	*/

	
	
		
	 CKEDITOR.disableAutoInline = false;
	
     $("div[contenteditable='true']" ).each(function( index ) {

        var id = $(this).attr('id');
        var node = $(this).data('node');
        var content_id = $(this).data('content_id');
       
        CKEDITOR.inline( id, {
            extraPlugins: 'sourcedialog',
            on: {
                blur: function( event ) {

                    var data = event.editor.getData();
           
                    var request = jQuery.ajax({
                        url: "/front/update",
                        type: "POST",
                        data: {
                        	node: node,
                            content : data,
                            content_id : content_id
                        },
                        dataType: "html"
                    });
                }
            }
        });
    });
});