# Vector Blog Platform #

2014 Application Foundation written by Chris Santala in Laravel 4.2

### Features ###

* Rich front end text editing with CKEditor or Markitup
* User Authentication with Roles
* Elegant ORM
* Blade Template Engine with Layouts, Sub Views, and logic
* AJAX contact form control
* Environment Detection with .env.php
* Custom Quotes Package Design (in Workbench)
* Composer Integrated
* Migrations / MySQL
* MiniTracker User Activity Monitoring
* Sluggable Article Views (slugged title)
* Modern Responsive Design Template
* Prism Syntax Highlighting
* jQuery Form Validation, misc scripts
* Version 1.0
[staging.vector.ablitica.com](http://staging.vector.ablitica.com)

### Contact ###

* csantala@gmail.com

## SETUP ##
* create vector database
* composer install
* php artisan migrate --package="toddish/verify"
* php artisan db:seed --class=VerifyUserSeeder
* php artisan migrate